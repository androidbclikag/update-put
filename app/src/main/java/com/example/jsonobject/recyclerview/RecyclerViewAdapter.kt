package com.example.jsonobject.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.jsonobject.models.ItemModel
import com.example.jsonobject.R
import kotlinx.android.synthetic.main.item_layout.view.*
import kotlinx.android.synthetic.main.item_layout.view.idTextView

class RecyclerViewAdapter(private val dataSet: ArrayList<ItemModel.UserModel>) :
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {


            val items = dataSet[adapterPosition]

            itemView.idTextView.text = items.id.toString()
            itemView.emailTextView.text = items.email
            itemView.firstNameTextView.text = items.firstName
            itemView.lastNameTextView.text = items.lastName
            Glide.with(itemView)
                .load(items.avatar)
                .placeholder(R.mipmap.ic_launcher_round)
                .into(itemView.avatarImageView)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
    )

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()
    }


}