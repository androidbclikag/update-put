package com.example.jsonobject.recyclerview

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jsonobject.R
import com.example.jsonobject.models.UnknownModel
import kotlinx.android.synthetic.main.unknown_layout.view.*

class RecyclerViewAdapterForUnknownUser(private val dataSet: ArrayList<UnknownModel.Model>):RecyclerView.Adapter<RecyclerViewAdapterForUnknownUser.MyViewHolder>() {
    inner class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val items = dataSet[adapterPosition]
            itemView.idTextView.text = items.id.toString()
            itemView.nameTextView.text = items.name
            itemView.yearTextView.text = items.year.toString()
            itemView.setBackgroundColor(Color.parseColor(items.color))
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =  MyViewHolder (LayoutInflater.from(parent.context).inflate(
        R.layout.unknown_layout, parent, false))

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.onBind()
    }


}