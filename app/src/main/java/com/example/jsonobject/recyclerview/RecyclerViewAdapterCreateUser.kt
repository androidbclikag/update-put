package com.example.jsonobject.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jsonobject.R
import com.example.jsonobject.models.CreateModel
import kotlinx.android.synthetic.main.activity_update.view.*
import kotlinx.android.synthetic.main.create_user.view.*

class RecyclerViewAdapterCreateUser(private val dataSet: ArrayList<CreateModel>) :
    RecyclerView.Adapter<RecyclerViewAdapterCreateUser.MyViewHolder>() {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val items = dataSet[adapterPosition]
            items.name = itemView.nameEditText.text.toString()
            items.job = itemView.jobEditText.text.toString()
            itemView.idTextViewCreate.text = items.id
            itemView.createdAtTextView.text = items.createdAt

        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.create_user, parent, false))

    override fun getItemCount(): Int = dataSet.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()
    }


}