package com.example.jsonobject.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log.d

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.jsonobject.apirequest.CallBack
import com.example.jsonobject.apirequest.HttpRequest
import com.example.jsonobject.R
import com.example.jsonobject.models.ItemModel
import com.example.jsonobject.recyclerview.RecyclerViewAdapter

import kotlinx.android.synthetic.main.activity_main.*

import org.json.JSONObject
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private val items = ArrayList<ItemModel.UserModel>()
    private lateinit var adapter: RecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUp()

    }

    private fun setUp() {

        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items)
        recyclerView.adapter = adapter
        getUsers()
        d("before", items.size.toString())
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = true
            refresh()
            d("refresh", items.size.toString())
            Handler().postDelayed({
                swipeRefresh.isRefreshing = false
                getUsers()
                adapter.notifyDataSetChanged()
                d("after", items.size.toString())
            }, 5000)
        }
    }

    private fun refresh() {
        items.clear()


    }


    private fun parseJson(body: String) {

        val itemModel = ItemModel()
        val json = JSONObject(body)
        if (json.has("page"))
            itemModel.page = json.getInt("page")
        if (json.has("per_page"))
            itemModel.perPage = json.getInt("per_page")
        if (json.has("total"))
            itemModel.total = json.getInt("total")
        if (json.has("total_pages"))
            itemModel.totalPages = json.getInt("total_pages")

        val jasonData = json.getJSONArray("data")

        for (i in 0 until jasonData.length()) {
            val data = jasonData[i] as JSONObject
            val user = ItemModel.UserModel()
            user.id = data.getInt("id")
            user.email = data.getString("email")
            user.firstName = data.getString("first_name")
            user.lastName = data.getString("last_name")
            user.avatar = data.getString("avatar")
            itemModel.data.add(user)
            items.add(user)
            adapter.notifyDataSetChanged()
        }


    }

    private fun getUsers() = HttpRequest.getRequest(
        HttpRequest.USERS,
        object : CallBack {
            override fun onFailure(body: String) {

            }

            override fun onResponse(response: String) {
                parseJson(response)
            }
        })


}
