package com.example.jsonobject.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.jsonobject.apirequest.CallBack
import com.example.jsonobject.apirequest.HttpRequest
import com.example.jsonobject.R
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        signUpButton.setOnClickListener { register() }
    }

    private fun register(){
        val parameters = mutableMapOf<String, String>()
        val email = emailEditTextSU.text.toString()
        val password = passwordEditTextSU.text.toString()
        val confirm = confirmPasswordEditTextSU.text.toString()
        if (password == confirm){

            parameters["email"] = email
            parameters["password"] = password
            HttpRequest.postRequest(
                HttpRequest.REGISTER,
                parameters,
                object : CallBack {
                    override fun onResponse(response: String) {
                        Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()

                    }

                    override fun onFailure(body: String) {
                        Toast.makeText(applicationContext, "fail", Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(body: String, message: String) {
                        errorTextView.text = message
                        Toast.makeText(applicationContext, body, Toast.LENGTH_SHORT).show()
                    }
                })

        }
        else
            Toast.makeText(applicationContext, "Can`t confirm, wrong password", Toast.LENGTH_SHORT).show()






}


}
