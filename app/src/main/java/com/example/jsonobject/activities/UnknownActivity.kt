package com.example.jsonobject.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.jsonobject.apirequest.CallBack
import com.example.jsonobject.apirequest.HttpRequest
import com.example.jsonobject.R
import com.example.jsonobject.models.UnknownModel
import com.example.jsonobject.recyclerview.RecyclerViewAdapter
import com.example.jsonobject.recyclerview.RecyclerViewAdapterForUnknownUser
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.recyclerView
import kotlinx.android.synthetic.main.activity_unknown.*

class UnknownActivity : AppCompatActivity() {
    private val items = ArrayList<UnknownModel.Model>()
    private lateinit var adapter: RecyclerViewAdapterForUnknownUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unknown)
        init()
        startSignInButton.setOnClickListener {
            val intent = Intent(applicationContext,  LogIn::class.java)
            startActivity(intent)
        }
    }


    private fun init() {
        recyclerViewUnknown.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapterForUnknownUser(items)
        recyclerViewUnknown.adapter = adapter
        getUnknown()
    }


    private fun getUnknown() {

        HttpRequest.getRequest(
            HttpRequest.UNKNOWN,
            object : CallBack {
                override fun onResponse(response: String) {
                    val unknownModel = Gson().fromJson(response, UnknownModel::class.java)
                    val listOfModel = unknownModel.data
                    for (index  in 0 until listOfModel.size){
                        val data = listOfModel[index]
                        val model = UnknownModel.Model()
                        model.id = data.id
                        model.name = data.name
                        model.color = data.color
                        model.year = data.year
                        model.pantoneValue = data.pantoneValue
                        items.add(model)
                        adapter.notifyDataSetChanged()
                    }

                }
            })
    }

}
