package com.example.jsonobject.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.jsonobject.R
import com.example.jsonobject.apirequest.CallBack
import com.example.jsonobject.apirequest.HttpRequest
import com.example.jsonobject.models.CreateModel
import com.example.jsonobject.recyclerview.RecyclerViewAdapterCreateUser
import kotlinx.android.synthetic.main.activity_update.*
import kotlinx.android.synthetic.main.create_user.*
import org.json.JSONObject

class UpdateUser : AppCompatActivity() {
    private val items = ArrayList<CreateModel>()
    private lateinit var adapter: RecyclerViewAdapterCreateUser
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)
        init()
        goToUnknown.setOnClickListener {
            val intent = Intent(this, UnknownActivity::class.java)
            startActivity(intent)
        }
    }

    private fun init() {
        recyclerViewCreateUSer.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapterCreateUser(items)
        recyclerViewCreateUSer.adapter = adapter
        addUserButton.setOnClickListener {
            addUser()
        }
    }

    private fun addUser() {
            addUserButton.visibility = View.GONE
        val name = nameEditText.text.toString()
        val job = jobEditText.text.toString()
        val parameters = mutableMapOf<String, String>()
        parameters["name"] = name
        parameters["job"] = job

        items.add(0, CreateModel("", "", name, job))
        adapter.notifyDataSetChanged()

        HttpRequest.postRequest(HttpRequest.UPDATE, parameters, object :CallBack{
            override fun onResponse(response: String) {
                addUserButton.visibility = View.VISIBLE
                val json = JSONObject(response)
                val id = json.getString("id")
                val createdAt = json.getString("id")
                val size = items.size - 1
                val changedItem = items[size]
                changedItem.id = id
                changedItem.createdAt = createdAt
                adapter.notifyItemInserted(0)

            }
        })

    }
}
