package com.example.jsonobject.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.jsonobject.apirequest.CallBack
import com.example.jsonobject.apirequest.HttpRequest
import com.example.jsonobject.R
import kotlinx.android.synthetic.main.activity_log_in.*

class LogIn : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        signInButton.setOnClickListener { logIn() }
        signUpButton.setOnClickListener {
            val intent = Intent(applicationContext, RegisterActivity::class.java)
            startActivity(intent)

        }
    }


    private fun logIn() {
        val parameters = mutableMapOf<String, String>()
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()) {
            parameters["email"] = email
            parameters["password"] = password
            HttpRequest.postRequest(
                HttpRequest.LOGIN,
                parameters,
                object : CallBack {
                    override fun onFailure(body: String) {

                        Toast.makeText(applicationContext, "fail", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(response: String) {
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                        Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()


                    }

                    override fun onError(body: String, message: String) {
                        errorTextView.text = message
                        Toast.makeText(applicationContext, body, Toast.LENGTH_SHORT).show()
                    }


                })
        }
        else
            Toast.makeText(applicationContext, "please fill all fields", Toast.LENGTH_SHORT).show()

    }


}
