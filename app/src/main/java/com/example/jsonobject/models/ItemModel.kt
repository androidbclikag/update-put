package com.example.jsonobject.models


class ItemModel {
    var page: Int = 0
    var perPage: Int = 0
    var total: Int = 0
    var totalPages: Int = 0
    var data = ArrayList<UserModel>()

    class UserModel {
        var id: Int = 0
        var email: String = ""
        var firstName: String = ""
        var lastName: String = ""
        var avatar: String = ""

    }
}