package com.example.jsonobject.models

import com.google.gson.annotations.SerializedName

class UnknownModel {

    var page = 0
    @SerializedName("per_page")
    var perPage = 0
    var total = 0
    @SerializedName("total_pages")
    var totalPages = 0
    var data = ArrayList<Model>()

    class Model {
        var id = 0
        var name = ""
        var year = 0
        var color = ""
        @SerializedName("pantone_value")
        var pantoneValue = ""
    }


}